public class Board{
	private Square[][] tictactoeBoard;
	public Board() {
		this.tictactoeBoard=new Square[3][3]; 
		for(int i=0;i<tictactoeBoard.length;i++){
			for(int j=0;j<tictactoeBoard[i].length;j++){
			 tictactoeBoard[i][j]=Square.BLANK;
			}
		}
	}
	public String toString() {
		String printMess="";
		for(int i=0;i<tictactoeBoard.length;i++){
			for(int j=0;j<tictactoeBoard[i].length;j++){
			printMess+=tictactoeBoard[i][j]+" ";
			}
			printMess+="\n";
		}
		return printMess;
	}

	public boolean placeToken(int row,int col,Square playerToken) {
		if(row<0 || row>2 || col<0||col>2) {
			return false;
		}else if(tictactoeBoard[row][col]==Square.BLANK){
			tictactoeBoard[row][col]=playerToken;
			return true;
		}else{
			return false;
		}
	}
		
	public boolean checkIfFull(){
		boolean checkTorF=false;
		int counter=0;
		for(int i=0;i<tictactoeBoard.length;i++){
			for(int j=0;j<tictactoeBoard[i].length;j++){
				if(tictactoeBoard[i][j]==Square.BLANK){
					checkTorF=false;
				}else{
					counter++;
					if(counter==9)
						return true;
				}
			}
		}
		return checkTorF;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken) {
		for(int i=0;i<tictactoeBoard.length;i++){
			int winCount=0;
			for(int j=0;j<tictactoeBoard[i].length;j++){
				if(tictactoeBoard[i][j]==playerToken){
					winCount++;
					if(winCount==3){
						System.out.println("hor");
						return true;
					}
				}	
			}
		}
		return false;
	}
	private boolean checkIfWinningVertical(Square playerToken) {
		for(int j=0;j<tictactoeBoard.length;j++){
			int winCount=0;
			for(int i=0;i<tictactoeBoard[j].length;i++){
				if(tictactoeBoard[i][j]==playerToken){
					winCount++;
					if(winCount==3){
						return true;
					}
				}	
			}
		}
		return false;
	}
	public boolean checkIfWinning(Square playerToken) {
		if(checkIfWinningHorizontal(playerToken)==true || checkIfWinningVertical(playerToken)==true) {
			return true;
		}else{
			return false;
		}
	}
}			