import java.util.Scanner;
public class TicTacToeGame {
	public static void main(String... args){
		Scanner sc=new Scanner(System.in);	
		System.out.println("Welcome Player");
	
		Board newPlayer=new Board();
		boolean gameOver=false;
		int player=0;
		Square playerToken=Square.X;
	
		player++;
		while(gameOver==false) {
			System.out.println("Player "+player+ " turn.");
			if(player==1){
				playerToken=Square.X;
			}else{
				playerToken=Square.O;
			}
			
			
				System.out.println(newPlayer);
				System.out.println("enter row number");
				int row=sc.nextInt();
				System.out.println("enter column number");
				int col=sc.nextInt();
				while (newPlayer.placeToken(row,col,playerToken)==false) {
					System.out.println("These inputs dont work please try again.");
					System.out.println("enter row number");
					row=sc.nextInt();
					System.out.println("enter column number");
					col=sc.nextInt();
				}
					
			if(newPlayer.checkIfWinning(playerToken)==true){
				System.out.println("Player "+player+" is the winner!");
				gameOver=true;
			}else if(newPlayer.checkIfFull()==true) {
				System.out.println("it's a tie!");
				gameOver=true;
			}else{
				player++;
				if(player>2){
					player=1;
				}
			}
		}
		System.out.println("game overrrrr");
		System.out.println(newPlayer);
	}
}				
		